﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

	// Movement variables
	public float runSpeed;
	public float walkSpeed;

	Rigidbody myRB;
	Animator myAnim;
	bool facingRight;

	// Jumping variables
	bool grounded = false;
	Collider[] groundCollisions;
	float groundCheckRadius = 0.2f;
	public LayerMask groundLayer;
	public Transform groundedCheck;
	public float jumpForce;
	public float maxJumpSpeed;

	// Use this for initialization
	void Start () {
		myRB = GetComponent<Rigidbody> ();
		myAnim = GetComponent<Animator> ();
		facingRight = true;
	}
	
	// Update is called once per frame (may be called faster or slow depending on frame rate)
	void Update () {
		
	}

	// Called on a fixed rate, after physics has run
	void FixedUpdate() {

		if (grounded && Input.GetAxis ("Jump") > 0) {
			grounded = false;
			myAnim.SetBool ("isGrounded", grounded);
			myRB.AddForce (new Vector3(0, jumpForce, 0));

			if (myRB.velocity.magnitude > runSpeed) {
				myRB.velocity = myRB.velocity.normalized * maxJumpSpeed;
			}

		}

		// Check if grounded
		groundCollisions = Physics.OverlapSphere(groundedCheck.position, groundCheckRadius, groundLayer);

		if (groundCollisions.Length > 0)
			grounded = true;
		else
			grounded = false;

		myAnim.SetBool ("isGrounded", grounded);

		// Rotate character in the correct direction
		float move = Input.GetAxis ("Horizontal");
		myAnim.SetFloat ("speed", Mathf.Abs (move));

		// Character sneaking
		bool isSneaking = (Input.GetAxisRaw("Fire3") > 0) ? true : false;
		myAnim.SetBool ("isSneaking", isSneaking);

		float moveSpeed = isSneaking && grounded ? walkSpeed : runSpeed;
		myRB.velocity = new Vector3 (move * moveSpeed, myRB.velocity.y, 0);

		if (move > 0 && !facingRight) {
			Flip ();
		} else if (move < 0 && facingRight) {
			Flip ();
		}



	}

	void Flip() {
		facingRight = !facingRight;
//		Vector3 theRotation = transform.localPosition;
//		theRotation.y = theRotation.y * -1;
		Vector3 aRotation = new Vector3 (0, 180, 0);
		transform.Rotate (aRotation);
	}
}
